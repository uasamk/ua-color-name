# README #

# ua-color-name

### Convert UA branded color names to HEX color values

### Example

```js
var uaColorName = require('ua-color-name')
uaColorName('uared') // => "#ab0520"
```

### Get meta data about a color
```js
uaColorName.get('uared')
// =>
{
    value: "#ab0520",
    name: "uared",
    maincolor: true
}
```

## API

### .get(name)
Returns object with the matching name value.

### .get.primary([name])
Returns an array of objects which are primary colors of the UA brand.
name - optional, returns an object with the matching name value so long as it is also a primary UA branded color.

### .get.neutral([name])
Returns an array of objects which are neutral colors of the UA brand.
name - optional, returns an object with the matching name value so long as it is also a neutral UA branded color.

### .get.secondary([name])
Returns an array of objects which are secondary colors of the UA brand.
name - optional, returns an object with the matching name value so long as it is also a secondary UA branded color.

### .get.legacy([name])
Returns an array of objects which are legacy colors of the UA brand.
name - optional, returns an object with the matching name value so long as it is also a legacy UA branded color.

### .all()
Returns an array of all color objects

## License
MIT