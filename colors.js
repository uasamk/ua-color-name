'use strict'

module.exports = [
    {
        "name":"bloom",
        "value": "#ef4056",
        "primary": true
    },
    {
        "name":"uared",
        "value": "#ab0520",
        "primary": true
    },
    {
        "name":"chili",
        "value": "#8b0015",
        "primary": true
    },
    {
        "name":"sky",
        "value": "#81d3eb",
        "primary": true
    },
    {
        "name":"oasis",
        "value": "#378dbd",
        "primary": true
    },
    {
        "name":"azurite",
        "value": "#1e5288",
        "primary": true
    },
    {
        "name":"arizonablue",
        "value": "#0c234b",
        "primary": true
    },
    {
        "name":"midnight",
        "value": "#001c48",
        "primary": true
    },
	{
        "name":"white",
        "value": "#ffffff",
        "primary": true
    },
	{
        "name":"black",
        "value": "#000000",
        "primary": true
    },
    {
        "name":"coolgray",
        "value": "#e2e9e9",
        "neutral": true
    },
    {
        "name":"warmgray",
        "value": "#f4ede5",
        "neutral": true
    },
    {
        "name":"leaf",
        "value": "#70b865",
        "secondary": true
    },
    {
        "name":"river",
        "value": "#007d84",
        "secondary": true
    },
    {
        "name":"silver",
        "value": "#9eabae",
        "secondary": true,
        "legacy": true
    },
    {
        "name":"silvertint",
        "value": "#eef1f1",
        "secondary": true
    },
    {
        "name":"mesa",
        "value": "#a95c42",
        "secondary": true
    },
    {
        "name":"ash",
        "value": "#403635",
        "legacy": true
    },
    {
        "name":"ashint",
        "value": "#f7f9f9",
        "legacy": true
    },
    {
        "name":"sage",
        "value": "#4a634e",
        "legacy": true
    },
    {
        "name":"copper",
        "value": "#a95c42",
        "legacy": true
    }
];
