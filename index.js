'use strict'

/**
 * Module dependencies
 */
var colors = require('./colors')

var primColors = colors.filter(function(color){
  return !! color.primary
})

var neutColors = colors.filter(function(color){
  return !! color.neutral
})

var secColors = colors.filter(function(color){
  return !! color.secondary
})

var legacyColors = colors.filter(function(color){
  return !! color.legacy
})

/**
 * Get color value for a certain name.
 * @param name {String}
 * @return {String} Hex color value
 * @api public
 */

module.exports = function(name) {
  var color = module.exports.get(name)
  return color && color.value
}

/**
 * Get color object.
 *
 * @param name {String}
 * @return {Object} Color object
 * @api public
 */

module.exports.get = function(name) {
  name = name || ''
  name = name.trim().toLowerCase()
  return colors.filter(function(color){
    return color.name.toLowerCase() === name
  }).pop()
}

/**
 * Get all color object.
 *
 * @return {Array}
 * @api public
 */

module.exports.all = module.exports.get.all = function() {
 return colors
}

/**
 * Get color object main colors only.
 *
 * @return {Array}
 * @api public
 */

module.exports.get.primary = function(name) {
  if (!name) return primColors
  name = name || ''
  name = name.trim().toLowerCase()
  return primColors.filter(function(color){
    return color.name.toLowerCase() === name
  }).pop()
}

/**
 * Get color object neutral colors only.
 *
 * @return {Array}
 * @api public
 */

module.exports.get.neutral = function(name) {
  if (!name) return neutColors
  name = name || ''
  name = name.trim().toLowerCase()
  return neutColors.filter(function(color){
    return color.name.toLowerCase() === name
  }).pop()
}

/**
 * Get color object secondary colors only.
 *
 * @return {Array}
 * @api public
 */

module.exports.get.secondary = function(name) {
  if (!name) return secColors
  name = name || ''
  name = name.trim().toLowerCase()
  return secColors.filter(function(color){
    return color.name.toLowerCase() === name
  }).pop()
}

/**
 * Get color object legacy colors only.
 *
 * @return {Array}
 * @api public
 */

module.exports.get.legacy = function(name) {
  if (!name) return legacyColors
  name = name || ''
  name = name.trim().toLowerCase()
  return legacyColors.filter(function(color){
    return color.name.toLowerCase() === name
  }).pop()
}