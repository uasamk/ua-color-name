'use strict'

var test = require('tape')

var colors = require('./index.js')

test('maps color names to HEX values', function(t) {
  t.plan(3)
  t.equal(colors('arizonablue'), '#0c234b')
  t.equal(colors('midnight'), '#001c48')
  t.equal(colors('bloom'), '#ef4056')
})

test('meta data about a color', function(t) {
  t.plan(2)
  t.deepEqual(colors.get('uared'), {
    name: "uared",
    value: "#ab0520",
    maincolor: true
  })
  t.deepEqual(colors.get('leaf'), {
    name: "leaf",
    value: "#70b865",
    secondary: true
  })
})